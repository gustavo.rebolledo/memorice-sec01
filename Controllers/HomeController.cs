﻿using MemoriceApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

using MemoriceApp.Models.ViewModel;

namespace MemoriceApp.Controllers
{
    public class HomeController : Controller
    {
        Image image = new Image() { Path = "/images/interrogacion.jpg" };

       List<Image> images = new List<Image>() { 
           new Image() { Path = "/images/dragon1.png" },
           new Image() { Path = "/images/dragon3.png" },
           new Image() { Path = "/images/dragon1.png" },
           new Image() { Path = "/images/dragon2.png" },
           new Image() { Path = "/images/dragon3.png" },
           new Image() { Path = "/images/dragon2.png" },
           new Image() { Path = "/images/dragon1.png" },
           new Image() { Path = "/images/dragon2.png" },
           new Image() { Path = "/images/dragon3.png" },
       };

        public HomeController(ILogger<HomeController> logger)
        {
        }

        public IActionResult Index()
        {
            HomeIndexViewModel model = new HomeIndexViewModel();
            model.Image = image;
            model.Images = images;
            return View(model);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult GetImage(int index)
        {
            var image = images[index];

            return Json(image);
        }
    }


}
