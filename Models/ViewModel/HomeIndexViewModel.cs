﻿using System.Collections.Generic;

namespace MemoriceApp.Models.ViewModel
{
    public class HomeIndexViewModel
    {
        public Image Image { get; set; }

        public List<Image> Images { get; set; }
    }
}
